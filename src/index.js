import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./Module";
import Styled from "./Styled";
import Header from "./components/Header";
import Carousel from "./components/Carousel";
import Card from "./components/Card";
import Collapse from "./components/Collapse";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Header />
    <Carousel />
    <Module />
    <Card />
    <Collapse />
    <Styled />
  </React.StrictMode>
);
