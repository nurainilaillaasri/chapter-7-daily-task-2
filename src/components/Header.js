import React from "react";
import "antd/dist/antd.css";
import "./style.css";
import { PageHeader } from "antd";
// import Dropdown from "./Dropdown";

const header = {
  height: '80px',
  // color: 'white',
  background: '#364d79',
};

export default () => (
  <PageHeader style={header}
    title="Selamat Datang"
    subtitle="This is a subtitle"
  />
)

