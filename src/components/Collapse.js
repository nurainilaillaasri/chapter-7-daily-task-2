import { Collapse } from 'antd';
import "./style.css";

const { Panel } = Collapse;

const text = `
  lorem ipsum dolor sit amet, consectetur adipiscing elit. 
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

export default () => (
  <Collapse accordion className='accordion'>
    <Panel header="Apa saja syarat yang dibutuhkan" key="1">
      <p>{text}</p>
    </Panel>
    <Panel header="Apakah ada biaya antar-jemput" key="2">
      <p>{text}</p>
    </Panel>
    <Panel header="Bagaimana jika terjadi kecelakaan" key="3">
      <p>{text}</p>
    </Panel>
  </Collapse>
);