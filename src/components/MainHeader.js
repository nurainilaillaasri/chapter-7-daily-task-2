import React from "react";
import "antd/dist/antd.css";
import Header from "./Header";
import Dropdown from "./Dropdown";

const MainHeader = (props) => {
  const { title } = props;

  return (
    <div className="header" style={{ width: "16rem" }}>
      <a><Header title={title} /></a>
      <div><Dropdown /></div>
    </div>
  )
}

export default MainHeader;