import { Card, Col, Row } from 'antd';
import "./style.css";
import Modal from "./Modal";

const imagetStyle = {
  width: '100%',
}

export default () => (
  <div className="site-card-wrapper">
    <Row gutter={16}>
      <Col span={8}>
        <Card title="Mobil" bordered={false}>
          <img
            style={imagetStyle}
            src="https://c8.alamy.com/comp/2CC1F5J/traffic-jam-street-road-graphic-color-city-landscape-sketch-illustration-vector-2CC1F5J.jpg"
            alt=""
          />
          <Modal />
        </Card>
      </Col>
      <Col span={8}>
        <Card title="Mobil" bordered={false}>
          <img
            style={imagetStyle}
            src="https://c8.alamy.com/comp/2CC1F5J/traffic-jam-street-road-graphic-color-city-landscape-sketch-illustration-vector-2CC1F5J.jpg"
            alt=""
          />
          <Modal />
        </Card>
      </Col>
      <Col span={8}>
        <Card title="Mobil" bordered={false}>
          <img
            style={imagetStyle}
            src="https://c8.alamy.com/comp/2CC1F5J/traffic-jam-street-road-graphic-color-city-landscape-sketch-illustration-vector-2CC1F5J.jpg"
            alt=""
          />
          <Modal />
        </Card>
      </Col>
    </Row>
  </div>
);