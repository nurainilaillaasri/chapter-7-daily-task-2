import React from "react";
import "antd/dist/antd.css";
import style from "./style.css";
import { Card, Carousel } from "antd";

const contentStyle = {
  height: '500px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

const imagetStyle = {
  width: '100%',
}

export default () => (
  <Carousel autoplay>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://media.istockphoto.com/vectors/street-in-public-park-with-nature-landscape-and-building-background-vector-id886746048?k=20&m=886746048&s=170667a&w=0&h=qIssTZ0_h6yemuTxpzBXvea8iLSSmFU6L3ZNWMJknlo="
          alt=""
        />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://thumbs.dreamstime.com/b/cute-cartoon-horizontal-mountain-landscape-road-beatiful-nature-concept-flat-style-vector-illustration-169911772.jpg"
          alt=""
        />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://img.freepik.com/free-vector/automobile-with-location-pin-road-online-ordering-taxi-car-sharing-concept-mobile-transportation-carsharing-service-mountains-river-landscape-background-flat-horizontal-banner_48369-28060.jpg?w=2000"
          alt=""
        />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://thumbs.dreamstime.com/b/urban-traffic-office-buildings-wallpaper-horizontal-vector-design-vehicle-car-mountain-modern-life-urban-traffic-banner-108735712.jpg"
          alt=""
        />
      </h3>
    </div>
  </Carousel>
);